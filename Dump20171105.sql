CREATE DATABASE  IF NOT EXISTS `dms` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `dms`;
-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: dms
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.17.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `apply`
--

DROP TABLE IF EXISTS `apply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `type` varchar(45) DEFAULT NULL,
  `reason` varchar(45) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `uploader` varchar(45) DEFAULT NULL,
  `reviewers` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apply`
--

LOCK TABLES `apply` WRITE;
/*!40000 ALTER TABLE `apply` DISABLE KEYS */;
INSERT INTO `apply` VALUES (1,'vsftpd','conf',NULL,'0e326324-7326-4fde-8795-424a3cff3497.conf','admin',NULL,0,NULL,NULL),(2,'vsftpd','conf',NULL,'d87916a1-85a2-4357-9792-212f8d2b378a.conf','admin',NULL,0,NULL,NULL),(3,'vsftpd','conf',NULL,'7db6b0ea-3960-4abb-b529-5672e4268703.conf','admin',NULL,0,NULL,NULL),(4,'vsftpd','conf',NULL,'vsftpd.2ac150ad-d713-497b-b6e0-3a0c4bad5f99.conf','admin',NULL,0,NULL,NULL),(5,'nginx','conf',NULL,'nginx.e7a453ad-5bdd-4404-9efe-6f322e909d6b.conf','admin',NULL,0,NULL,NULL),(6,'vsftpd','conf',NULL,'vsftpd.edc3f8f5-870f-4220-a3c0-3efbf8674aa5.conf','admin',NULL,0,NULL,NULL),(7,'nginx','conf',NULL,'nginx.0a70e558-20c3-4b6e-a3e6-d37f93472325.conf','admin',NULL,0,NULL,NULL),(8,'learning','conf',NULL,'learning.e649a53b-bb90-4725-b7ee-bf1b12e2c20e.conf','admin',NULL,0,NULL,NULL),(9,'nginx','conf',NULL,'nginx.5c66d153-4ba5-4d1d-9833-3f9b7bbf140c.conf','admin',NULL,0,NULL,NULL),(10,'nginx','conf',NULL,'nginx.8e4c1d39-59fe-427b-8268-be239136d7be.conf','admin',NULL,0,NULL,NULL),(11,'nginx','conf',NULL,'nginx.60864b83-6b5c-418a-9809-5bd5eb2e4ae8.conf','admin',NULL,0,NULL,NULL),(12,'learning','conf',NULL,'learning.2c9119dd-a130-4d91-80a4-747c89918c69.conf','admin',NULL,0,NULL,NULL),(13,'learning','conf',NULL,'learning.6e285fb5-d1a8-4671-bb88-e1761f92bbd1.conf','admin',NULL,0,NULL,NULL),(14,'nginx','conf',NULL,'nginx.8cb3bfc4-4c37-43a0-abbe-ba2b536a4ff8.conf','admin',NULL,0,NULL,NULL),(15,'nginx','conf',NULL,'nginx.7076d6d1-320a-416c-8ae9-4cb360a8028b.conf','admin',NULL,0,NULL,NULL),(16,'vsftpd','conf',NULL,'vsftpd.0b448513-4adc-4a44-9b8b-af32f4d5a5b6.conf','admin',NULL,0,NULL,NULL),(17,'nginx','conf',NULL,'nginx.83f17afe-f8d1-4a0f-8284-21ba0cc9f50b.conf','admin',NULL,0,NULL,NULL);
/*!40000 ALTER TABLE `apply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  `update_time` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,0,'计算机',1,NULL,NULL),(2,1,'软件工程',1,NULL,NULL),(3,0,'技术',1,NULL,NULL),(4,3,'231',1,NULL,NULL);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file`
--

DROP TABLE IF EXISTS `file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `uploader` varchar(45) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  `update_time` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file`
--

LOCK TABLES `file` WRITE;
/*!40000 ALTER TABLE `file` DISABLE KEYS */;
INSERT INTO `file` VALUES (3,1,'123456','sdf','dasfa',3,'admin',NULL,NULL),(4,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL),(5,NULL,NULL,NULL,NULL,2,NULL,NULL,NULL),(6,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL),(7,NULL,NULL,NULL,NULL,0,'管理员1号',NULL,NULL),(8,NULL,'',NULL,NULL,1,'管理员1号',NULL,NULL),(9,NULL,'',NULL,NULL,1,'管理员1号',NULL,NULL),(10,NULL,'',NULL,NULL,0,'管理员1号',NULL,NULL),(11,NULL,'',NULL,NULL,0,'管理员1号',NULL,NULL),(12,NULL,NULL,NULL,NULL,2,'管理员1号',NULL,NULL),(13,NULL,'learning','conf',NULL,0,'管理员1号',NULL,NULL),(14,NULL,'vsftpd','conf',NULL,0,'管理员1号',NULL,NULL),(15,NULL,'deploy','sh','deploy.ba5450b0-5344-46de-a8ed-c372b3ad8876',0,'管理员1号',NULL,NULL),(16,NULL,'learning','conf','learning.01ef4d5f-b84d-46de-b17d-3e79c1e09695',1,'管理员1号',NULL,NULL),(17,NULL,'th','jpeg','th.ff2ff79b-59ce-426d-a334-dc0672c67901',0,'管理员1号',NULL,NULL),(18,NULL,'nginx','conf','bc872a41-d712-44ab-a840-8323b58fc1cb',0,'管理员1号',NULL,NULL);
/*!40000 ALTER TABLE `file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `telephone` varchar(45) DEFAULT NULL,
  `uploader` varchar(45) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  `update_time` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','E10ADC3949BA59ABBE56E057F20F883E','管理员1号','315992237@qq.com','18172301432',NULL,2,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'dms'
--

--
-- Dumping routines for database 'dms'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-05 19:50:55
