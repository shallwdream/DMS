package com.dms.common;

public class Const {
    public static final String CURRENT_USER = "currentUser";

    public static final String UPLOAD_FAIL="uploadFail";

    public static final String EMAIL = "email";
    public static final String USERNAME = "username";

    public interface Role{
        int ROLE_CUSTOMER=0;    //普通用户
        int ROLE_ADMIN=1;   //普通管理员
        int ROLE_SUPPER_ADMIN=2;    //超级管理员
    }
}
