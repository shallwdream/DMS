package com.dms.service;

import com.dms.common.ServerResponse;
import com.dms.pojo.File;
import com.dms.vo.FileDetailVo;
import com.github.pagehelper.PageInfo;
import org.springframework.web.multipart.MultipartFile;

public interface IFileService {

    /**
     * 添加文件
     *
     * @param file
     * @return
     */
    ServerResponse saveOrUpdateFile(File file);

    /**
     * 修改文件状态
     *
     * @param fileId
     * @param status
     * @return
     */
    ServerResponse<String> setFileStatus(Integer fileId, Integer status);

    /**
     * 文件详情
     *
     * @param fileId
     * @return
     */
    ServerResponse<FileDetailVo> manageFileDetail(Integer fileId);

    /**
     * 获取文件列表
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse getFileList(int pageNum, int pageSize);

    /**
     *搜索文件
     * @param fileName
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse<PageInfo> searchFile(String fileName, int pageNum, int pageSize);

    /**
     * 文件上传
     *
     * @param file
     * @param path
     * @return
     */
    String upload(MultipartFile file, String path,String uploadFileName);

    /**
     * 文件下载
     *
     * @param localPath
     * @param fileName
     * @return
     */
    boolean download(String localPath, String fileName,String fileNewName);

    /**
     * 文件移动
     *
     * @param fileName
     * @return
     */
    String moveFie(String fileName);

    /**
     *
     * @param file
     * @return
     */
    ServerResponse<File> SaveFilesData(File file);
}
