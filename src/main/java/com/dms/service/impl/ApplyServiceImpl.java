package com.dms.service.impl;

import com.dms.common.ResponseCode;
import com.dms.common.ServerResponse;
import com.dms.dao.ApplyMapper;
import com.dms.dao.FileMapper;
import com.dms.pojo.Apply;
import com.dms.pojo.User;
import com.dms.service.IApplyService;
import com.dms.service.IFileService;
import com.dms.vo.ApplyListVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("iApplyService")
public class ApplyServiceImpl implements IApplyService {

    private Logger logger = LoggerFactory.getLogger(ApplyServiceImpl.class);

    @Autowired
    private ApplyMapper applyMapper;
    @Autowired
    private IFileService iFileService;

    @Override
    public ServerResponse addApply(Apply apply) {
        if (apply == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "参数输入错误");
        }

        int rowCount = applyMapper.insert(apply);
        if (rowCount > 0) {
            ServerResponse.createBySuccess("Apply添加成功");
        }

        return ServerResponse.createByErrorMessage("Apply添加失败");
    }

    @Override
    public ServerResponse getApplyListByStatus(Integer status, int pageNum, int pageSize) {
        if (status == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "参数输入错误");
        }

        PageHelper.startPage(pageNum, pageSize);
        List<Apply> applyList = applyMapper.selectListByStatus(status);
        if (CollectionUtils.isEmpty(applyList)) {
            logger.info("未找到该分类");
        }

        List<ApplyListVo> applyListVos = Lists.newArrayList();
        for (Apply apply : applyList) {
            ApplyListVo applyListVo = assembleAppleList(apply);
            applyListVos.add(applyListVo);
        }
        PageInfo pageResult = new PageInfo(applyList);

        pageResult.setList(applyListVos);

        return ServerResponse.createBySuccess(pageResult);
    }

    @Override
    public ServerResponse updateApplyStatus(Integer applyId,String address, Integer status, String reason) {
        if (status == null || StringUtils.isBlank(reason)) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "参数错误");
        }
        Apply apply = new Apply();
        apply.setId(applyId);
        apply.setStatus(status);
        apply.setReason(reason);

        int rowCount = applyMapper.updateByPrimaryKeySelective(apply);
        if (rowCount > 0) {
            //移动文件
            iFileService.moveFie(address);
            //写入文件表
            return ServerResponse.createBySuccess("Apply更新成功");
        }

        return ServerResponse.createByErrorMessage("Apply更新失败");
    }

    @Override
    public ServerResponse getApplyList(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Apply> applyList = applyMapper.selectList();
        if (CollectionUtils.isEmpty(applyList)) {
            logger.info("未找到该分类");
        }

        List<ApplyListVo> applyListVos = Lists.newArrayList();
        for (Apply apply : applyList) {
            ApplyListVo applyListVo = assembleAppleList(apply);
            applyListVos.add(applyListVo);
        }
        PageInfo pageResult = new PageInfo(applyList);

        pageResult.setList(applyListVos);


        return ServerResponse.createBySuccess(pageResult);
    }

    /**
     * 封装Apple
     *
     * @param apply
     * @return
     */
    private ApplyListVo assembleAppleList(Apply apply) {
        ApplyListVo applyListVo = new ApplyListVo();
        applyListVo.setId(apply.getId());
        applyListVo.setName(apply.getName());
        applyListVo.setType(apply.getType());
        applyListVo.setUploader(apply.getUploader());
        applyListVo.setStatus(apply.getStatus());
        return applyListVo;
    }


}
