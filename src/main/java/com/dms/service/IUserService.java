package com.dms.service;

import com.dms.common.ServerResponse;
import com.dms.pojo.User;
import com.github.pagehelper.PageInfo;

public interface IUserService {
    /**
     * 用户登录
     * @param username
     * @param password
     * @return
     */
    ServerResponse<User> login(String username, String password);

    /**
     * 添加用户
     * @param user
     * @return
     */
    ServerResponse<String> addUser(User user);

    /**
     * 检查合法性
     * @param str
     * @param type
     * @return
     */
    ServerResponse<String> checkValid(String str, String type);

    /**
     * 搜索用户列表
     * @param username
     * @return
     */
    ServerResponse<String> selectQuestion(String username);

    /**
     * 重置用户密码
     * @param passwordOld
     * @param passwordNew
     * @param user
     * @return
     */
    ServerResponse<String> resetPassword(String passwordOld, String passwordNew, User user);

    /**
     * 更新用户信息
     * @param user
     * @return
     */
    ServerResponse<User> update_information(User user);

    /**
     * 得到用户详细信息
     * @param userId
     * @return
     */
    ServerResponse<User> getInformation(Integer userId);

    /**
     * 检查是否为管理员
     * @param user
     * @return
     */
    ServerResponse checkAdminRole(User user);

    /**
     * 通过登录账号或者用户名或者手机号获取用户信息
     * @param username
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServerResponse<PageInfo> getUserByUsernameOrList(String username, int pageNum, int pageSize);

}
