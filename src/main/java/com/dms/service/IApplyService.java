package com.dms.service;

import com.dms.common.ServerResponse;
import com.dms.pojo.Apply;

public interface IApplyService {

    /**
     * 添加Apply
     *
     * @param apply
     * @return
     */
    ServerResponse addApply(Apply apply);

    /**
     * 根据Apply状态
     *
     * @param status
     * @return
     */
    ServerResponse getApplyListByStatus(Integer status,int pageNum,int pageSize);

    /**
     * 修改Apply状态
     *
     * @return
     */
    ServerResponse updateApplyStatus(Integer applyId,String address,Integer status, String reason);

    ServerResponse getApplyList(int pageNum,int pageSize);



}