package com.dms.controller.backend;

import com.dms.common.Const;
import com.dms.common.ResponseCode;
import com.dms.common.ServerResponse;
import com.dms.pojo.User;
import com.dms.service.IApplyService;
import com.dms.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * 后台申请处理
 */
@Controller
@RequestMapping("/manage/apply")
public class ApplyManagerController {
    @Autowired
    private IUserService iUserService;

    @Autowired
    private IApplyService iApplyService;

    /**
     * 得到Apple列表
     *
     * @param session
     * @param status
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "list.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse getApplyListByStatus(HttpSession session,
                                               @RequestParam(value = "status", defaultValue = "0") Integer status,
                                               @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                               @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录,请登录");
        }
        if (iUserService.checkAdminRole(user).isSuccess()) {
//            return ServerResponse.createBySuccess(iApplyService.getApplyList(pageNum,pageSize));
            return ServerResponse.createBySuccess(iApplyService.getApplyListByStatus(0,pageNum,pageSize));
//            return null;
        } else {
            return ServerResponse.createByErrorMessage("无权限操作,需要管理员权限");
        }

    }

    /**
     * 更新apply状态
     * @param session
     * @param ApplyId
     * @param status
     * @param reason
     * @return
     */
    @RequestMapping(value = "set_apply_status.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse setApplyStatus(HttpSession session, Integer ApplyId,String address, Integer status, String reason) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录,请登录管理员");
        }

        //检查用户权限
        if (iUserService.checkAdminRole(user).isSuccess()) {
            //填充增加的业务逻辑
            return iApplyService.updateApplyStatus(ApplyId,address, status, reason);
        } else {
            return ServerResponse.createByErrorMessage("无权这样操作");
        }
    }
}
