package com.dms.controller.backend;

import com.dms.common.Const;
import com.dms.common.ServerResponse;
import com.dms.pojo.User;
import com.dms.service.IUserService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * 后台管理员管理
 */
@Controller
@RequestMapping("/manage/user")
public class UserManageController {
    @Autowired
    private IUserService iUserService;

    @RequestMapping(value = "login.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<User> login(String username, String password, HttpSession session) {
        ServerResponse<User> response = iUserService.login(username, password);
        if (response.isSuccess()) {
            User user = response.getData();
            if (user.getRole() >= Const.Role.ROLE_ADMIN) {
                //登录为管理员
                session.setAttribute(Const.CURRENT_USER, user);
                return response;
            } else {
                return ServerResponse.createByErrorMessage("不是管理员，无法登陆");
            }
        }

        return response;

    }

    /**
     * 添加用户
     *
     * @param
     * @return
     */
    @RequestMapping(value = "add_user.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<String> addUser(HttpSession session, String username, String password, String name, String email, String telephone, String uploader) {
        User currentUser = (User) session.getAttribute(Const.CURRENT_USER);
        if (currentUser == null) {
            return ServerResponse.createByErrorMessage("用户没有登录");
        }

        if (currentUser.getRole() == Const.Role.ROLE_ADMIN) {
            //登录为管理员
            User user = new User();
            user.setUsername(username);
            user.setPassword(password);
            user.setName(name);
            user.setEmail(email);
            user.setTelephone(telephone);
            user.setUploader(uploader);
            return iUserService.addUser(user);
        } else {
            return ServerResponse.createByErrorMessage("不是管理员，无权操作");
        }

    }

    /**
     * 根据账号or用户名or电话号码查询用户信息
     *
     * @param username
     * @param pi
     * @param ps
     * @return
     */
    @RequestMapping(value = "user_list.do", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse<PageInfo> list(HttpSession session,
                                         @RequestParam(value = "username", required = false) String username,
                                         @RequestParam(value = "pi", defaultValue = "1") int pi,
                                         @RequestParam(value = "ps", defaultValue = "10") int ps) {
        User currentUser = (User) session.getAttribute(Const.CURRENT_USER);
        if (currentUser == null) {
            return ServerResponse.createByErrorMessage("用户没有登录");
        }

        if (currentUser.getRole() >= Const.Role.ROLE_ADMIN) {
            return iUserService.getUserByUsernameOrList(username, pi, ps);
        } else {
            return ServerResponse.createByErrorMessage("不是管理员，无权操作");
        }

    }


}
