package com.dms.pojo;

import javax.xml.crypto.Data;
import java.util.Date;

public class Apply {
    private Integer id;

    private String name;

    private String type;

    private String address;

    private String reason;

    private String uploader;

    private String reviewers;

    private Integer status;

    private Date createTime;

    private Date updateTime;

    public Apply(Integer id, String name, String type, String address, String reason, String uploader, String reviewers, Integer status, Date createTime,Date updateTime){
        this.id=id;
        this.name=name;
        this.type=type;
        this.address=address;
        this.reason=reason;
        this.uploader=uploader;
        this.reviewers=reviewers;
        this.status=status;
        this.createTime=createTime;
        this.updateTime=updateTime;
    }


    public Apply(){
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getUploader() {
        return uploader;
    }

    public void setUploader(String uploader) {
        this.uploader = uploader;
    }

    public String getReviewers() {
        return reviewers;
    }

    public void setReviewers(String reviewers) {
        this.reviewers = reviewers;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
